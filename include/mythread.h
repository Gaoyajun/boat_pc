/*
 * This procedure is used to achieve the data transmission and communication with boat
 *
 * Copyright 2015 Yajun Gao (gyjun0230 at 163 dot com), NRSL, HIT Shenzhen, China, 
 * Copyright 2015 Ting Li (644638930 at qq dot com), NRSL, HIT Shenzhen, China
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MYTHREAD_H
#define MYTHREAD_H

#include <QThread>
#include <QtGlobal>
#include <QtDebug>
#include<QTime>
#include<QtMath>

#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>

#include<iostream>

#include<math.h>
#include <stdio.h>

class MyThread : public QThread
{
    Q_OBJECT
public:
    QString currentLat, currentLon,targetLat,targetLon;
    QByteArray requestData;
    QByteArray TxData;
    MyThread();

    void setMessage(const QString &message);
    void setPortnum(const QString &num);
    void setPortBaudRate(const QString &BaudRate);
    void stop();
    void startCom();
    void changeTxState(bool stat);
    void changeRxState(bool stat);
    void changeComState(bool stat);

    void boat_system_start_n();     //系统开启   才能再开启自主导航
    void boat_system_stop_n();

    void boat_mode_N();  //自主导航模式
    void boat_mode_P();   //遥控模式

signals:
    void request(const QString &s);
    void comRecive();
    void updateLocalSignal();

public slots:
    void getCurrentLoc(const QString & , const QString &);
    void getTargetLoc   (const QString & , const QString &);

protected:
    void run();

private:
    QString messageStr_;
    QString portnum_;
    QString portBautRate_;
    volatile bool com_opened_;
    volatile bool stopped_;
    volatile bool sendEvent_;
    volatile bool receiveEvent_;
    volatile bool boatState_;    //系统开启状态  再去开自主导航
    volatile bool boatMode_;   //定义船是工作在自动导航模式还是遥控模式

};

#endif // MYTHREAD_H
