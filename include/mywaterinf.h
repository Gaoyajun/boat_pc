/*
 * This procedure is used to achieve the data transmission and communication with boat
 *
 * Copyright 2015 Yajun Gao (gyjun0230 at 163 dot com), NRSL, HIT Shenzhen, China, 
 * Copyright 2015 Ting Li (644638930 at qq dot com), NRSL, HIT Shenzhen, China
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef WATERINFCLASS
#define WATERINFCLASS

#include <QLabel>
#include <QDialog>
#include <QLineEdit>
#include <QGridLayout>
#include <QPushButton>

class MyWaterInf: public QDialog
{
    Q_OBJECT
public:
    MyWaterInf();

    QLineEdit       *lineEditWaterInf;
    QLabel            *laberWaterInf;
    QPushButton *pushbuttonQuit;

private:

signals:

public slots:
    void getWaterInf(const QString &);         //Send按钮单击事件连接的槽函数，目的为发射infoSend信号
};
#endif // WATERINFCLASS

