/*
 * This procedure is used to achieve the data transmission and communication with boat
 *
 * Copyright 2015 Yajun Gao (gyjun0230 at 163 dot com), NRSL, HIT Shenzhen, China, 
 * Copyright 2015 Ting Li (644638930 at qq dot com), NRSL, HIT Shenzhen, China
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MYMAP_H
#define MYMAP_H

#include <QLineEdit>
#include <QLabel>
#include <QPushButton>
#include <QGridLayout>
#include <QWebFrame>
#include <QWebView>
#include <QMessageBox>
#include <QDialog>

class Mymap : public QDialog
{
    Q_OBJECT
public:
    Mymap();

    QWebView *baiduMap;

    QLineEdit *LineEditTargetLat;
    QLineEdit *LineEditTargetLon;
    QLineEdit *LineEditCurrentLat;
    QLineEdit *LineEditCurrentLon;
    QLineEdit *LineEditIdentification;

    QLabel *LabelTargetLat;
    QLabel *LabelTargetLon;
    QLabel *LaberCurrentLat;
    QLabel *LaberCurrentLon;

    QPushButton *pushbuttonSetIdentification;
    QPushButton *pushbuttonSendTargetLocal;
    QPushButton *pushbuttonQuit;
    QPushButton *pushbuttonGetDistance;
private:
    int radiusSize_;
    QString strCity_;

signals:
    void sendTargetLoc(const QString & , const QString &); //定义消息发送信号
public slots:
    void on_webView_loadFinished(bool);
    void on_map_button_size_clicked();
    void on_map_Button_clicked();
    //void boat_loc();    //实时的定位船的当前位置和目标位置
    void updateLocal();
    void on_getDistance_clicked();

    void getBoatState(bool);

    void emitSignalSendTarget();      //Send按钮单击事件连接的槽函数，目的为发射sendTarget信号

    void getCurrentLoc(const QString & , const QString &);
};

#endif // MAPCLASS_H

