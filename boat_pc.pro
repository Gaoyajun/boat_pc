#
# This procedure is used to achieve the data transmission and communication with boat
#
# Copyright 2015 Yajun Gao (gyjun0230 at 163 dot com), NRSL, HIT Shenzhen, China, 
# Copyright 2015 Ting Li (644638930 at qq dot com), NRSL, HIT Shenzhen, China
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#-------------------------------------------------
#
# Project created by QtCreator 2015-11-3T13:28:37
#
#-------------------------------------------------

QT       += core gui

QT       +=webkitwidgets

QT       += serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets


TARGET    = IntoRobot_ship
TEMPLATE  = app


SOURCES  += src/main.cpp\
            src/mainwindow.cpp \
            src/mythread.cpp \
            src/mywaterinf.cpp \
    	    src/mymap.cpp \


HEADERS  += include/mainwindow.h \
            include/mythread.h \
    	    include/mymap.h \
            include/mywaterinf.h

FORMS    += mainwindow.ui

RC_ICONS  = image/ab.ico
