/*
 * This procedure is used to achieve the data transmission and communication with boat
 *
 * Copyright 2015 Yajun Gao (gyjun0230 at 163 dot com), NRSL, HIT Shenzhen, China, 
 * Copyright 2015 Ting Li (644638930 at qq dot com), NRSL, HIT Shenzhen, China
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "include/mywaterinf.h"

MyWaterInf::MyWaterInf()
{
    setWindowTitle("MyWaterInformation");

    laberWaterInf     = new QLabel("水温信息：");
    lineEditWaterInf = new QLineEdit("******");
    pushbuttonQuit  =new QPushButton("Quit");

    QGridLayout     *layout   = new QGridLayout(this);
    layout->addWidget(laberWaterInf,0,0);
    layout->addWidget(lineEditWaterInf,0,1);
    layout->addWidget(pushbuttonQuit,1,1);

    this->resize(300,200);

    connect(pushbuttonQuit,SIGNAL(clicked()),this,SLOT(close()));

}

void MyWaterInf::getWaterInf(const QString &str)
{
    lineEditWaterInf->setText(str);
}
